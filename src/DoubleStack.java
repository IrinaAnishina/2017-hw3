import java.util.LinkedList;

public class DoubleStack
{
	
	public static void main(String[] argum){
	DoubleStack m = new DoubleStack();
	System.out.println(m);
	m.push(1);
	System.out.println(m);
	m.push(3);
	System.out.println(m);
	m.push(6);
	System.out.println(m);
	m.push(2);
	System.out.println(m);
	m.op("/");
	System.out.println(m);
	m.op("*");
	System.out.println(m);
	m.op("-");
	System.out.println(m);
	double result = m.pop();
	System.out.println(m);
	System.out.println(result);
	DoubleStack copy = m;
	System.out.println(copy.equals(m));
	System.out.println(m);
	System.out.println(copy);
	try {
		copy = (DoubleStack) m.clone();
	} catch (CloneNotSupportedException e) {
	}
	System.out.println(copy.equals(m));
	System.out.println(m);
	System.out.println(copy);
	m.push(6);
	System.out.println(copy.equals(m));
	System.out.println(m);
	System.out.println(copy);
	m.pop();
	System.out.println(copy.equals(m));
	System.out.println(m);
	System.out.println(copy);
	String prog = "2 3 + 4 * 10 /";
	if (argum.length > 0) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < argum.length; i++) {
			sb.append(argum[i]);
			sb.append(" ");
		}
		prog = sb.toString();
	}
	System.out.println(prog + "\n " + String.valueOf(interpret(prog)));
}

	private LinkedList<Double> stack;
	
	//constructor
	DoubleStack()
	{
		stack= new LinkedList <Double>();
	}

	//Copy of the stack
	@Override
    public Object clone() throws CloneNotSupportedException {

	       DoubleStack clone = new DoubleStack();

	       //clone.stack = (LinkedList<Double>) stack.clone();
	       clone.stack.addAll(this.stack);
			
			return clone;

	    }

	// Check whether the stack is empty
		public boolean stEmpty() {
			return stack.isEmpty();
		}

		// Adding an element to the stack
		public void push(double a) {
			stack.push(a);
		}

		// Removing an element from the stack
		public double pop() {
			 if (stack.size()==0){

				 throw new RuntimeException("Not enough of elements!");

		 	   }
		 	   return stack.pop();
		} // pop

	public void op(String s)
	{
		if(stack.size() < 2)
		{
			throw new RuntimeException("Not enough of elements!");
		}

		double b = stack.pop();
		double a = stack.pop();
		switch(s.charAt(0))
		{
			case '+': stack.push(a + b); break;
			case '-': stack.push(a - b); break;
			case '*': stack.push(a * b); break;
			case '/': stack.push(a / b); break;
			default: throw new RuntimeException("Such operation is not allowed");
		}
	}	// op

	//Reading the top without removing it

	public double tos() {
		 if (stack.size()==0){

	 		   throw new RuntimeException("Not enough of elements!");

	 	   }
		 else
		return stack.peek();
	}

	//Check whether two stacks are equal
	@Override
	public boolean equals(Object o)
	{
		DoubleStack tmp = (DoubleStack) o;

		return stack.equals(tmp.stack);
	}

	//Conversion of the stack to string (top last)
	@Override
	public String toString()
	{
		StringBuffer buf = new StringBuffer();

		for(int i = stack.size() - 1; i >= 0; i--)
		{
			buf.append(stack.get(i));
		}

		return buf.toString();
	}

	
	/*Write a method to calculate the value of an arithmetic expression pol in RPN 
	 * using this stack type. The result must be double value of the expression or throwing a 
	 *    RuntimeException in case the expression is not correct. 
	 *    Expression is not correct if it contains
	 *     illegal symbols, 
	 *    leaves redundant elements on top of stack 
	 *    or causes stack underflow. 
	*	Example. DoubleStack.interpret ("2. 15. -") should return -13. 
			*/
	 public static double interpret (String pol) {
	 		DoubleStack stack = new DoubleStack();
	 		//http://stackoverflow.com/questions/225337/how-do-i-split-a-string-with-any-whitespace-chars-as-delimiters
	 		String[] elements = pol.trim().split("\\s+");
	 		int n = 0, o = 0;
	 		for (int i = 0; i < elements.length; i++) {
				if (elements[i].equals("/") || elements[i].equals("*") || elements[i].equals("+") || elements[i].equals("-")) {
					try {
						stack.op(elements[i]);
						o++;
					} catch (Exception e) {
						throw new RuntimeException("Not enough operands in expression! Expression \"" + pol +"\"");
					}
				} else {
					try {
						double d = Double.valueOf((elements[i]));
						stack.push(d);
						n++;
					} catch (Exception e) {
						throw new RuntimeException("Cannot convert argument \"" + elements[i] + "\" to double in expression " + pol);
					}
				}

			}
			if((n - 1) != o){
				throw new RuntimeException("Stack is not balanced. Operands :" + n + " Operators :" + o);
			}

			return stack.pop();
	    }
	 }